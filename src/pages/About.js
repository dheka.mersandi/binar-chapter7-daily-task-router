import logo from '../logo.svg';
import '../App.css';

function About() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Ini adalah Halaman About
        </p>
      </header>
    </div>
  );
}

export default About;
